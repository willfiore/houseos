let keyboard_enabled = false;
let keyboard_txt = "";
let keyboard_suggestion = "";
let keyboard_cid = 0;

let keyboard_callback = 0; // fn
let keyboard_suggestion_callback = 0; //fn

let keyboard_pos_left_row = 0;
let keyboard_pos_left_col = 0;
let keyboard_pos_right_row = 0;
let_keyboard_pos_right_col = 0;

$("#keyboard").css({
    left:"calc(11vw + 25%)",
    top:"30%"
});

function ui_open_keyboard(title, cid, callback, suggestion_callback)
{
    $("#keyboard").css({
        opacity:1
    });
    $("#keyboard_dimmer").css({
        opacity:1 
    });
    $("#keyboard_title").html(title || "");
    keyboard_callback = callback || 0;
    keyboard_suggestion_callback = suggestion_callback || 0;
    
    keyboard_txt = "";
    kb_refresh_field();
    
    keyboard_enabled = true;
    keyboard_cid = cid;
}

function ui_close_keyboard()
{
    $("#keyboard").css({
        opacity:0
    });
    $("#keyboard_dimmer").css({
        opacity:0
    });
    keyboard_enabled = false;
}

function kb_select() {
    let oldSelected = $(".kb_selected").removeClass("kb_selected");
    
    let newSelectedLeft = $("#keyboard_holder").children(".keyboard_row").eq(keyboard_pos_left_row)
    .children().eq(keyboard_pos_left_col).addClass("kb_selected left");

    let newSelectedRight = $("#keyboard_holder").children(".keyboard_row").eq(keyboard_pos_right_row)
    .children().eq(keyboard_pos_right_col).addClass("kb_selected right");
}

function kb_press_key(side) {
    playSound("keyboard_click");
    if (side == 0) {
        keyboard_txt += $(".kb_selected.left").text();
    }
    else {
        keyboard_txt += $(".kb_selected.right").text();
    }
    kb_refresh_field();
}

function kb_backspace() {
    playSound("keyboard_click2");
    keyboard_txt = keyboard_txt.slice(0, -1);
    kb_refresh_field();
}

function kb_space() {
    playSound("keyboard_click");
    if (!keyboard_txt.length) return;
    if (keyboard_txt[keyboard_txt.length-1].trim() == "") return;
    
    keyboard_txt += " ";
    kb_refresh_field();
}

function kb_clear()
{
    playSound("keyboard_click2");
    keyboard_txt = "";
    kb_refresh_field();
}

function kb_return() {
    if ($.isFunction(keyboard_callback) && keyboard_txt) {
        keyboard_callback(keyboard_txt);
    }
    ui_close_keyboard();
}

function kb_cancel() {
    ui_close_keyboard();
}

let suggestionTimer = 0;
function kb_refresh_field(dontSuggest) {
    
    $("#keyboard_field").empty();
    $("<span/>", {
            html: keyboard_txt + "_",
            class: "user_input"
    }).appendTo($("#keyboard_field"));
    
    if ($.isFunction(keyboard_suggestion_callback) && !dontSuggest) {
        clearInterval(suggestionTimer);
        suggestionTimer = setTimeout(function() {
            keyboard_suggestion_callback(keyboard_txt, kb_received_suggestions);
        }, 0);
    }
}

function kb_received_suggestions(original, suggestionObj)
{    
    if (keyboard_txt != original) return;
    
    keyboard_suggestion = suggestionObj;

    if (!keyboard_txt.trim().length) {
        keyboard_suggestion.suggestion = "";
        return;
    }
    
    $("#keyboard_field").empty();
    $("<span/>", {
        html: keyboard_txt + "_",
        class: "user_input"
    }).appendTo($("#keyboard_field"));
    $("<span/>", {
        html: " " + keyboard_suggestion.suggestion,
        class: "suggestion_post"
    }).appendTo($("#keyboard_field"));
    return;
}

function kb_complete_suggestion()
{
    //if (!keyboard_suggestion) return;
    
//    if (keyboard_suggestion.extraArtists) {
//        for (var i = 0; i < keyboard_suggestion.extraArtists.length; i++) {
//            let artistName = removeAccents(keyboard_suggestion.extraArtists[i].name.toLowerCase());
//            
//            if (keyboard_txt.indexOf(keyboard_completed_words) >= 0) {
//                keyboard_txt = keyboard_completed_words + keyboard_suggestion.suggestion.toLowerCase() + " ";
//                keyboard_completed_words = keyboard_txt;
//                kb_refresh_field(true);
//                return;
//            }
//            
////            if (keyboard_txt.indexOf(artistName) >= 0) {
////                keyboard_txt = artistName + " " + keyboard_suggestion.suggestion.toLowerCase() + " ";
////                kb_refresh_field(true);
////                return;
////            }
//        }
//    }
    
    let replacementText = keyboard_suggestion.suggestion.trim();
    if (!replacementText) return;
    
    let extraText = [];
    for (let i = 0; i < keyboard_suggestion.extra.length; i++) {
        let index = keyboard_txt.indexOf(keyboard_suggestion.extra[i]);
        if(index >= 0) {
            extraText.push({txt:keyboard_suggestion.extra[i], index: index});
        }
    }
    
    extraText.sort(function(a, b) {
            if (a.index > b.index)
                return -1;
            if (a.index < b.index)
                return 1;
            return 0;
        });
    
    for (let i = 0; i < extraText.length; i++) {
        replacementText = extraText[i].txt + " " + replacementText;
    }
    
    keyboard_txt = replacementText + " ";
    kb_refresh_field(true);
}