let PAGE = {
    GAMES: 0,
    SPOTIFY: 1,
    TV: 2
}

let navbar_current = 0;
let current_page = 0;
let page_list = [];

//   let pageObject = {
//       page: 0,
//       contexts: []
//   };
//
//   let contextObject = {
//       object = jqueryObject,
//       selected = jqueryObject
//};

function ui_setClock() {
    let time = new Date();
    let h = time.getHours();
    let m = time.getMinutes();
    $(".clock").html((h > 12 ? h - 12 : h) + ":" + (m < 10 ? "0" : "") + m + (h < 12 ? " AM" : " PM"));
    
    setTimeout(ui_setClock, 1000);
}
ui_setClock();

function ui_navbar_select(id)
{
    if (id < 0 || id > 2) return;
    navbar_current = id;
    $("#navbar_slider").css("transform", "translateY(calc(-" + id + "*12vw))");
    
    let selectedElement = $("#navbar_slider").children().eq(id);
    selectedElement.css({"opacity": 1, "-webkit-filter": "grayscale(0)"});
    $("#navbar_slider").children().not(selectedElement).css({"opacity": 0.3, "-webkit-filter": "grayscale(0.8)"});
    
    playSound("menu_click1");
}

function ui_spotify_set_nowplaying(data)
{
    $("#pg_sp1_nowplaying_trackname").html(data.trackName);
    $("#pg_sp1_nowplaying_artistname").html(data.artistName);
}

function ui_spotify_set_seekbar(data)
{
    let percentage = 100*(data.currentTime / data.duration);
    let seekbar = $("#pg_sp1_nowplaying_seekbar");
    seekbar.css({
        width: percentage + 0.2 + "vw"
    })
}

function ui_spotify_set_featured_playlists()
{
    $("#pg_sp1_fp_title").html(spotify_data.featuredPlaylists.message);
    $("#pg_sp1_fp_holder").empty();
    
    let playlists = spotify_data.featuredPlaylists.playlists.items;
    for(let i = 0; i < playlists.length; i++) {
        let item = $("<div/>", {
            class: "pg_sp1_main_playlist_item selectable"
        }).appendTo("#pg_sp1_fp_holder");
        
        item.data("href", playlists[i].href);
        item.css({
            "background-image": "url(" + playlists[i].images[0].url + ")"
        })
    }
}

let uniquePlaylistImages = {
    "My Shazam Tracks": "images/unique/shazam.png",
    "Pres": "images/unique/pres.png"
};

function ui_spotify_set_my_playlists()
{
    $("#pg_sp1_my_title").html("Will's Playlists");
    
    let playlists = spotify_data.myPlaylists.items;
    for (let i = 0; i < playlists.length; i++) {
        let item = $("<div/>", {
            class: "pg_sp1_main_playlist_item selectable"
        }).appendTo("#pg_sp1_my_holder");
        
        item.data("href", playlists[i].href);
        item.css({
            "background-image": "url(" + playlists[i].images[0].url + ")"
        });
        
        if (uniquePlaylistImages[playlists[i].name]) {
            item.css({
                "background-image": "url(" + uniquePlaylistImages[playlists[i].name] + ")"
            });
        }
        
        if(playlists[i].owner.id.indexOf("spotify") == -1) {
            let textOverlay = $("<div/>", {
                class: "pg_sp1_main_playlist_item_text",
                text: playlists[i].name
            }).appendTo(item);
        }
    }
}

function ui_navbar_slide(toggle)
{
    if(toggle) {
        $("#wrapper").css("transform", "translateX(11vw)");
    } else {
        $("#wrapper").css("transform", "translateX(0)");
    }
}

function ui_loadpage()
{
    let pgid;    
    switch (navbar_current) {
        case PAGE.GAMES: pgid="pg_games";
            break;
        case PAGE.SPOTIFY: pgid= "pg_sp1";
            break;
        case PAGE.TV: pgid="pg_tv";
            break;
        default: return;
    }
    
    let pageHolder = $("#" + pgid);
    pageHolder.css({
        "visibility": "visible",
        "opacity": 1,
        "transform": "translateX(0)"
    });
    $(".page").not(pageHolder).css({
        "visibility": "hidden",
        "opacity": 0,
        "transform": "translateX(-4vw)"
    });
    
    // First time loading the page
    if (!page_list[navbar_current]) {
        let defaultSelected = pageHolder.find(".selectable").first().addClass("selected");
        
        page_list[navbar_current] = {
            page: navbar_current,
            contexts: [{
                element: $("#" + pgid),
                selected: defaultSelected
            }]
        };
    }
    
    current_page = navbar_current;
    
}

function ui_current_page() {
    return page_list[current_page];
}

function ui_current_context() {
    return ui_current_page().contexts[ui_current_page().contexts.length - 1];
}

function ui_select(direction) {
    
    let selectable = ui_current_context().element.find(".selectable");
    let selected = ui_current_context().selected;
    
    if(!selected) return;
    
    let s = [];
    s[DIRECTION.LEFT] = 0;
    s[DIRECTION.RIGHT] = 0;
    s[DIRECTION.UP] = 0;
    s[DIRECTION.DOWN] = 0;
    
    if (ui_current_context().element.is($("#pg_sp1"))) {
        selectable.each(function(i) {
            if ($(this) == selected) return;
            // RIGHT
            if ($(this).offset().left > selected.offset().left && $(this).offset().top == selected.offset().top) {
                if (!s[DIRECTION.RIGHT] || selected.offset().left - $(this).offset().left > selected.offset().left - s[DIRECTION.RIGHT].offset().left) {
                    s[DIRECTION.RIGHT] = $(this);
                }
            }
            // LEFT
            else if ($(this).offset().left < selected.offset().left && $(this).offset().top == selected.offset().top) {
                if (!s[DIRECTION.LEFT] || selected.offset().left - $(this).offset().left < selected.offset().left - s[DIRECTION.LEFT].offset().left) {
                    s[DIRECTION.LEFT] = $(this);
                }
            }
            // UP
            if ($(this).offset().top < selected.offset().top) {
                if (!s[DIRECTION.UP] || ($(this).offset().top - selected.offset().top && 
                            selected.offset().top - $(this).offset().top <= selected.offset().top - s[DIRECTION.UP].offset().top &&
                           Math.abs(selected.offset().left - $(this).offset().left) < Math.abs(selected.offset().left - s[DIRECTION.UP].offset().left))) {
                    s[DIRECTION.UP] = $(this);
                }
            }
            // DOWN
            else if ($(this).offset().top > selected.offset().top) {
                if (!s[DIRECTION.DOWN] || ($(this).offset().top - selected.offset().top && 
                              selected.offset().top - $(this).offset().top >= selected.offset().top - s[DIRECTION.DOWN].offset().top &&
                              Math.abs(selected.offset().left - $(this).offset().left) < Math.abs(selected.offset().left - s[DIRECTION.DOWN].offset().left))) {
                    s[DIRECTION.DOWN] = $(this);
                }
            }
        });
    }
    else if (ui_current_context().element.is($("#pg_sp1_playlist_overlay"))) {
        if (selected.next().length)
            s[DIRECTION.DOWN] = selected.next();
        if (selected.prev().length)
            s[DIRECTION.UP] = selected.prev();
        
        let fastSkipAmount = 8;
        
        if (selected.nextAll().length)
            s[DIRECTION.RIGHT] = selected.nextAll().eq(Math.min(fastSkipAmount, selected.nextAll().length-1));
        
        let prevAllLength = selected.prevAll().length;
        if (prevAllLength)
            s[DIRECTION.LEFT] = selected.prevAll().eq(Math.min(fastSkipAmount, prevAllLength-1));
    }
    
    if (s[direction]) {
        selected.removeClass("selected");
        ui_current_context().selected = s[direction];
        ui_current_context().selected.addClass("selected");
        
        if (ui_current_context().element.is($("#pg_sp1"))) {
            ui_scroll_horizontal_container();
        }
        else if (ui_current_context().element.is($("#pg_sp1_playlist_overlay"))) {
            ui_scroll_vertical_container();
        }
        
        playSound("menu_click1");
    }
}

function ui_scroll_horizontal_container()
{
    let selected = ui_current_context().selected;
    let holder = selected.parent();
    holder.css({
        transform:"translateX(-" + selected.index()/(holder.children().length-1) * (holder.children().length * selected.innerWidth() - holder.parent().innerWidth()) + "px)"
    });
}

function ui_scroll_vertical_container()
{
    let selected = ui_current_context().selected;
    let holder = selected.parent();
    
    let numElementsVisible = (holder.parent().innerHeight() / selected.innerHeight());
    let insetTop = Math.floor(numElementsVisible/2) - 1;
    let insetBottom = Math.floor(numElementsVisible/2) + 1;
    
    let percentageOfMax = (selected.index()-insetTop)/(holder.children().length-1-insetTop-insetBottom);
    
    if (percentageOfMax < 0) percentageOfMax = 0;
    if (percentageOfMax > 1) percentageOfMax = 1;
    
    holder.css({
        transform:"translateY(-" + percentageOfMax * (holder.children().length * selected.innerHeight() - holder.parent().innerHeight()) + "px)"
    });
}

function ui_enter_selection()
{
    let selected = ui_current_context().selected;
    if (selected.hasClass("pg_sp1_main_playlist_item")) {
        ui_current_page().contexts.push({element: $("#pg_sp1_playlist_overlay"), loaded: false});
        spotify_api(selected.data("href"), ui_spotify_open_playlist_overlay);
    }
    else if (selected.hasClass("pg_sp1_playlist_overlay_track")) {
        spotify_play(selected.data("uri"));
        spotify_setQueue(spotify_data.lastOpenPlaylist, selected.index());
    }
}

function ui_close_current_context()
{
    let context = ui_current_page().contexts.pop();
    context.element.removeClass("open");
    context.element.find(".selectable").remove();
}

function ui_spotify_open_search_results_overlay(data)
{    
    console.log(data);
    let headerImage = $("#pg_sp1_playlist_overlay_header_image");
    headerImage.css({
        "background-image": "url(images/icons/search.png)"
    });
    
    $("#pg_sp1_playlist_overlay_header_name").html("Search results for \"" + data.searchTerm + "\"");
    $("#pg_sp1_playlist_overlay_header_desc").html(data.tracks.items.length + " track" + (data.tracks.items.length == 1 ? "" : "s") + " found.");
    
    let trackHolder = $("#pg_sp1_playlist_overlay_tracks_holder .inner_scroller");
    
    trackHolder.empty();

    for (let i = 0; i < data.tracks.items.length; i++) {
        let track = data.tracks.items[i];
        if(!track) continue;

        let trackItem = $("<div/>", {
            class: "pg_sp1_playlist_overlay_track selectable " + ((i%2)?" odd":"")
        }).appendTo(trackHolder);
        
        if (i == 0 && !data.tracks.offset) {
            trackItem.addClass("selected");
            ui_current_context().selected = trackItem;
        }

        let trackName = $("<div/>", {
            class: "pg_sp1_playlist_overlay_track_name"
        }).appendTo(trackItem);
        trackName.html(track.name);

        let artistName = $("<div/>", {
           class: "pg_sp1_playlist_overlay_track_artist"
        }).appendTo(trackItem);

        let artistNameText = track.artists[0].name;
        for (let j = 1; j < track.artists.length; j++) {
            artistNameText += ", " + track.artists[j].name;
        }
        artistName.html(artistNameText);
        
        trackItem.data("uri", track.uri);
    }
    
    $("#pg_sp1_playlist_overlay").addClass("open");
    ui_scroll_horizontal_container();
    ui_current_context().loaded = true;
}
        
function ui_spotify_open_playlist_overlay(data)
{
    let headerImage = $("#pg_sp1_playlist_overlay_header_image");
    
    if (uniquePlaylistImages[data.name]) {
        headerImage.css({
            "background-image": "url(" + uniquePlaylistImages[data.name] + ")"
        });
    } else {
        headerImage.css({
        "background-image": "url(" + data.images[0].url + ")"
    });
        
    }
    $("#pg_sp1_playlist_overlay_header_name").html(data.name);
    if(data.description) {
        if(data.description.indexOf("Photo") >= 0) {
            data.description = data.description.substr(0, data.description.indexOf("Photo"))
        }
        if(data.description.indexOf("Image") >= 0) {
            data.description = data.description.substr(0, data.description.indexOf("Image"))
        }
        if(data.description.indexOf("Pic") >= 0) {
            data.description = data.description.substr(0, data.description.indexOf("Pic"))
        }
        if(data.description.indexOf("Picture") >= 0) {
            data.description = data.description.substr(0, data.description.indexOf("Picture"))
        }
        if(data.description.indexOf("Cover photo") >= 0) {
            data.description = data.description.substr(0, data.description.indexOf("Cover photo"))
        }
        
        // Remove hyperlinks
        data.description = data.description.replace(/<(?:.|\n)*?>/gm, '');
        
        $("#pg_sp1_playlist_overlay_header_desc").html(data.description);
    } else {
        $("#pg_sp1_playlist_overlay_header_desc").html("");
    }
    
    $("#pg_sp1_playlist_overlay_tracks_holder .inner_scroller").empty();
    
    spotify_data.lastOpenPlaylist = data;
    ui_spotify_playlist_load_tracks(data.tracks);
    
    $("#pg_sp1_playlist_overlay").addClass("open");
    ui_scroll_horizontal_container();
    ui_current_context().loaded = true;
}

function ui_spotify_playlist_load_tracks(data)
{
    if(!ui_current_context().element.is($("#pg_sp1_playlist_overlay"))) return;
    
    let trackHolder = $("#pg_sp1_playlist_overlay_tracks_holder .inner_scroller");

    for (let i = 0; i < data.items.length; i++) {
        let track = data.items[i].track;
        if(!track) continue;
        
        if (data.offset) {
            spotify_data.lastOpenPlaylist.tracks.items.push(data.items[i]);
        }

        let trackItem = $("<div/>", {
            class: "pg_sp1_playlist_overlay_track selectable " + ((i%2)?" odd":"")
        }).appendTo(trackHolder);
        
        if (i == 0 && !data.offset) {
            trackItem.addClass("selected");
            ui_current_context().selected = trackItem;
        }

        let trackName = $("<div/>", {
            class: "pg_sp1_playlist_overlay_track_name"
        }).appendTo(trackItem);
        trackName.html(track.name);

        let artistName = $("<div/>", {
           class: "pg_sp1_playlist_overlay_track_artist"
        }).appendTo(trackItem);

        let artistNameText = track.artists[0].name;
        for (let j = 1; j < track.artists.length; j++) {
            artistNameText += ", " + track.artists[j].name;
        }
        artistName.html(artistNameText);
        
        trackItem.data("uri", track.uri);
    }
    
    if (data.next) {
        spotify_api(data.next, ui_spotify_playlist_load_tracks);
    }
}

$(document).ready(function() {
});