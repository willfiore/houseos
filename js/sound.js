function playSound(s) {
    let sound = new Audio("sounds/" + s + ".wav");
    sound.volume = 0.2;
    sound.play();
}