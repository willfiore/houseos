// API authentication
let spotify_clientID = "c8b4b05f81c34a1ebbee20e67bb73b3e";
let spotify_clientSecret = "0d820e7852f34890b775b74f49858762";

// OAuth user authentication.
/////////////////////////////////////
let spotify_authToken = "AQBhoOkySHzga-jz--EZ55elngYo6hKmMGQ0xmFwb60adWPVU_Ljc9MN2CsdLilX6oiY4kI64mM0fPy5wk3hkCdsIkg1iuy3evwxe9GVPnWSosn5RhnCu1f-Zknxwd06Oqe37GY0lnCQYpc-LP9tvOOA21iRp4KP2gQyF-sPgQGtAtalswfxUUkKpIEknIJ5"

// Get new authtokens at 
// https://accounts.spotify.com/authorize?response_type=code&client_id=c8b4b05f81c34a1ebbee20e67bb73b3e&redirect_uri=http://homeOS.com/callback
let spotify_accessToken = "";
/////////////////////////////////////

/////////////////////////////////////
// Data we fetch!
/////////////////////////////////////
let spotify_data = {
    nowPlaying: {},
    playQueue: {},
    myPlaylists: {},
    featuredPlaylists: {},
    lastOpenPlaylist: {}
};

function spotify_auth(callback) {
    let options = {
        url: "https://accounts.spotify.com/api/token",
        headers: {
            "Authorization": "Basic " + (new Buffer(spotify_clientID + ":" + spotify_clientSecret).toString('base64'))
        },
        form: {
            grant_type: "refresh_token",
            refresh_token: localStorage.getItem("spotify_refreshToken")
        },
        json: true
    };

    httpRequest.post(options, function (e, res, body) {
        if (!e && res.statusCode === 200) {
            // If we have received another refresh token, update:
            if (body.refresh_token) {
                localStorage.spotify_refreshToken = body.refresh_token;
            }
            // Update our access token
            console.log("Spotify Authentication:", body);
            spotify_accessToken = body.access_token;
            // Reauth just before it expires.
            setTimeout(spotify_auth, (1000*body.expires_in - 60000));
            
            if (callback) {
                callback();
            }
            
            spotify_updateData();
        } else {
            console.log(res);
        }
    });
}

function spotify_api(link, callback) {
    
    // If we havent authed yet, try again soontm
    if (!spotify_accessToken) {
        setTimeout(function() {
            spotify_api(link, callback);
        }, 1000);
        return;
    }
    
    if (link.substr(0, 8) != "https://") {
        link = "https://api.spotify.com/v1/" + link;
    }
    
    let options = {
        url: link,
        headers: {
            "Authorization": "Bearer " + spotify_accessToken
        },
        json: true
    };

    httpRequest.get(options, function (e, res, body) {
        console.log("Spotify:", link, body);
        if (body.error) {
//                spotify_auth(function() {
//                    spotify_api(link, callback);
//                });
        }
        else {
            if (callback) {
                callback(body);
            }
        }
    });
}

function spotify_getTrackData(obj, callback)
{
    spotify_api("tracks/" + obj.uri.slice(14), function(res) {
        let data = {
            trackName: res.name,
            artistName: res.artists[0].name,
            albumUri: res.album.uri,
            artistUri: res.artists[0].uri,
            isLocal: false
        }
        
        for (let i = 1; i < res.artists.length; i++) {
            data.artistName += ", " + res.artists[i].name;
        }
        callback(data);
    });
}

function spotify_setNowPlaying(data) {
    spotify_data.nowPlaying = data;
    ui_spotify_set_nowplaying(data);
}

function spotify_makeLocalTrackData(data, callback) {
    let obj = {
        trackName: data.trackName,
        artistName: data.artistName,
        isLocal: true
    }
    
    spotify_data.nowPlaying = obj;
    callback(obj);
}

function spotify_updateData() {
    spotify_api("me/playlists", function(res) {
        spotify_data.myPlaylists = res;
        ui_spotify_set_my_playlists();
    });
    
    //spotify:user:spotify_uk_:playlist:6FfOZSAN3N6u7v81uS7mxZ
    
    spotify_api("browse/featured-playlists?country=gb", function(res) {
        spotify_data.featuredPlaylists = res;
        ui_spotify_set_featured_playlists();
        ui_navbar_select(1);
        ui_loadpage();
    });
}

function spotify_play(uri) {
    ws.send(KEY._PLAY, uri);
    console.log(uri);
}

function spotify_playNext() {
    if ($.isEmptyObject(spotify_data.playQueue)) return;
    
    if (spotify_data.playQueue.tracks.total <= spotify_data.playQueue.currentIndex+1) {
        spotify_data.playQueue.currentIndex = -1;
    }
    
    spotify_data.playQueue.currentIndex++;
    
    if (spotify_data.playQueue.tracks.items[spotify_data.playQueue.currentIndex].is_local) {
        spotify_playNext();
        return;
    }
    
    spotify_play(spotify_data.playQueue.tracks.items[spotify_data.playQueue.currentIndex].track.uri);
}

function spotify_setQueue(playlistObject, startingIndex, shuffle) {
    spotify_data.playQueue = playlistObject;
    spotify_data.playQueue.currentIndex = startingIndex;
    
    if(shuffle) {
        spotify_data.playQueue.tracks.items = shuffleArray(spotify_data.playQueue.tracks.items);
        spotify_data.playQueue.shuffle = true;
    } else {
        spotify_data.playQueue.shuffle = false;
    }
}

function spotify_shuffleOpenPlaylist()
{
    spotify_setQueue(spotify_data.lastOpenPlaylist, -1, true);
    spotify_playNext();
}

function spotify_getSuggestion(txt, callback)
{
    if (txt.length < 3) return;
    let searchDepth = 1;
    txt = txt.trim();
    txt = removeAccents(txt);
    
    let lastTypedWord = txt.split(" ").pop();
    
    let returnArray = [];
    
    function trimDelimiters(obj) {
        var delim = obj.suggestion.indexOf(" - ");
            obj.suggestion = obj.suggestion.substring(0, delim != -1 ? delim : obj.suggestion.length);
            delim = obj.suggestion.indexOf(" (");
            obj.suggestion = obj.suggestion.substring(0, delim != -1 ? delim : obj.suggestion.length);
            delim = obj.suggestion.indexOf(" / ");
            obj.suggestion = obj.suggestion.substring(0, delim != -1 ? delim : obj.suggestion.length);
            delim = obj.suggestion.indexOf(" [");
            obj.suggestion = obj.suggestion.substring(0, delim != -1 ? delim : obj.suggestion.length);
    //        delim = obj.suggestion.indexOf(",");
    //        obj.suggestion = obj.suggestion.substring(0, delim != -1 ? delim : obj.suggestion.length);
            
            obj.suggestion = removeAccents(obj.suggestion);
            obj.suggestion = obj.suggestion.trim().toLowerCase();
    }
    
    spotify_api("search?market=gb&type=track,artist&limit=" + searchDepth + "&q=" + txt + "*", function(res) {
        
        for (let i = 0; i < res.tracks.items.length; i++) {
            let track = res.tracks.items[i];
            let suggestionObj = {suggestion:"", extra:[], popularity:0, exact:false};
            
            suggestionObj.suggestion = track.name;
            suggestionObj.popularity = track.popularity;
            suggestionObj.exact = false;
            
            for (let j = 0; j < track.artists.length; j++) {
                suggestionObj.extra.push(track.artists[j].name.toLowerCase());
            }
            
            trimDelimiters(suggestionObj);
            returnArray.push(suggestionObj);
        }
        
        for (let i = 0; i < res.artists.items.length; i++) {
            let artist = res.artists.items[i];
            let suggestionObj = {suggestion:"", extra:[], popularity:0, exact:false};
            
            suggestionObj.suggestion = artist.name;
            suggestionObj.popularity = artist.popularity + 10;
            
            trimDelimiters(suggestionObj);
            returnArray.push(suggestionObj);
        }
        
        if (!returnArray.length) return;
        
        returnArray.sort(function(a, b) {
            if (a.popularity > b.popularity)
                return -1;
            if (a.popularity < b.popularity)
                return 1;
            return 0;
        });
        
        callback(txt, returnArray[0]);
    });
}

function spotify_getSearchResults(txt, callback)
{
    ui_current_page().contexts.push({element: $("#pg_sp1_playlist_overlay"), loaded: false});
    let searchDepth = 50;
    spotify_api("search?market=gb&type=track&limit=" + searchDepth + "&q=" + txt + "*", function(res) {
        res.searchTerm = txt.trim();
        
        // Filter shitty results
        for (var i = 0; i < res.tracks.items.length;) {
            let track = res.tracks.items[i];
            if (track.name.toLowerCase().indexOf("originally performed") >= 0) {
                res.tracks.items.splice(i, 1);
                continue;
            }
            
            let foundKaraokeArtist = false;
            for (var j = 0; j < track.artists.length; j++) {
                if (track.artists[j].name.toLowerCase().indexOf("karaoke") >= 0) {
                    foundKaraokeArtist = true;
                }
            }

            let foundCopyTrack = false;
            for (var j = 0; j < i; j++) {
                if (track.name != res.tracks.items[j].name) continue;
                if (track.artists.length != res.tracks.items[j].artists.length) continue;
                let foundBadArtist = false;
                for (var k = 0; k < track.artists.length; k++) {
                // loop through artists.. check for equality
                    if (track.artists[k].name == res.tracks.items[j].artists[k].name) {
                        foundBadArtist = true;
                    }
                }
                if (!foundBadArtist) continue;
            }
            
            if (foundKaraokeArtist || foundCopyTrack) {
                res.tracks.items.splice(i, 1);
            } else {
                i++;
            }
        }
        
        ui_spotify_open_search_results_overlay(res);
    });
}

$(document).ready(function() {
    spotify_auth(function() {
    });
});