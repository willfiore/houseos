let C_buttonID = {
    X: 0,
    CIRCLE: 1,
    SQUARE: 2,
    TRIANGLE: 3,
    LB: 4,
    RB: 5,
    LT: 6,
    RT: 7,
    SELECT: 8,
    START: 9,
    LS: 10,
    RS: 11,
    DPAD_UP: 12,
    DPAD_DOWN: 13,
    DPAD_LEFT: 14,
    DPAD_RIGHT: 15,
}

let DIRECTION = {
    UP:0,
    RIGHT:1,
    DOWN:2,
    LEFT:3
}

let gamepadState;
let C_buttonsDown = [[], [], [], []];
let C_axesPositions = [[], [], [], []];

let inputEnabled  = true;
let checkSpeed = 4;
let holdRepeatSpeed = 3 * (16/checkSpeed);
let holdWaitTime = 14 * (16/checkSpeed);

let checkCounter = 0;
function gamepadEvents() {
    gamepadState = navigator.getGamepads();
    
    for (let i = 0; i < gamepadState.length; i++) {
        let gamepad = gamepadState[i];
        if (gamepad == undefined) continue;
        
        for (let j = 0; j < gamepad.buttons.length; j++) {
            
            // If a button is pressed that wasn't before:
            if (gamepad.buttons[j].pressed === true && C_buttonsDown[i][j] <= 0) {
                C_onButtonPress(i, j);
            }
            // If a button is released that was pressed before:
            else if (gamepad.buttons[j].pressed === false && C_buttonsDown[i][j] > 0) {
                C_onButtonRelease(i, j);
            }
            
            if (gamepad.buttons[j].pressed === false) {
                C_buttonsDown[i][j] = 0;
            } else {
                // increase hold timer
                C_buttonsDown[i][j]++;
            }
            
            if (C_buttonsDown[i][j] % holdRepeatSpeed == holdRepeatSpeed-1 && C_buttonsDown[i][j] > holdWaitTime) {
                if (j >= 12 && j <= 15) {
                    C_onButtonPress(i, j);
                }
                if (j == C_buttonID.SQUARE && keyboard_enabled) {
                    C_onButtonPress(i, j);
                }
            }
            
            // Axes
            C_axesPositions[i] = gamepad.axes;
            
            checkCounter++
            // Keyboard Axes
            if (keyboard_enabled && !(checkCounter % 4)) {
                C_moveKeyboardCursor();
                checkCounter = 0;
            }
            
        }
    }
}

setInterval(gamepadEvents, checkSpeed);

function C_isButtonDownAnyController(bid) {
    for (let i = 0; i < gamepadState.length; i++) {
        if (C_buttonsDown[i][bid]) {
            return true;
        }
    }
    return false;
}

function C_onButtonPress(cid, bid) {
    
    //if (!document.hasFocus()) return;
    if (!inputEnabled)
    {
        return;
    }
    
    if(keyboard_enabled) {
        
        // Let anybody close the keyboard
        if (bid == C_buttonID.CIRCLE) {
            kb_cancel();
        }
        
        if (cid != keyboard_cid) return;
        
        if (bid == C_buttonID.LB) {
            kb_press_key(0);
        }
        else if(bid == C_buttonID.RB) {
            kb_press_key(1);
        }
        else if (bid == C_buttonID.SQUARE) {
            kb_backspace();
        }
        else if (bid == C_buttonID.TRIANGLE) {
            kb_space();
        }
        else if (bid == C_buttonID.X) {
            kb_return();
        }
        else if (bid == C_buttonID.DPAD_RIGHT) {
            kb_complete_suggestion();
        }
        else if (bid == C_buttonID.DPAD_LEFT) {
            kb_clear();
        }
        
        return;
    }
    
    // Navbar movement:
    if (C_isButtonDownAnyController(C_buttonID.LT)) {
        if (bid == C_buttonID.DPAD_DOWN) {
            ui_navbar_select(navbar_current + 1);
        }
        if (bid == C_buttonID.DPAD_UP) {
            ui_navbar_select(navbar_current - 1);
        }
    } else {
    
        // Selection navigation
        if (bid == C_buttonID.DPAD_RIGHT) {
            ui_select(DIRECTION.RIGHT);
        }
        else if (bid == C_buttonID.DPAD_LEFT) {
            ui_select(DIRECTION.LEFT);
        }
        else if (bid == C_buttonID.DPAD_DOWN) {
            ui_select(DIRECTION.DOWN);
        }
        else if (bid == C_buttonID.DPAD_UP) {
            ui_select(DIRECTION.UP);
        }
        else if (bid == C_buttonID.X) {
            ui_enter_selection();
        }        
        
        else if (bid == C_buttonID.CIRCLE) {
            if(ui_current_page().contexts.length > 1) {
                if (ui_current_context().loaded) {
                    ui_close_current_context();
                }
            }
        }
        
        // Spotify search
        if(ui_current_context().element.is($("#pg_sp1"))) {
           if (bid == C_buttonID.TRIANGLE) {
               ui_open_keyboard("Search for music", cid, spotify_getSearchResults, spotify_getSuggestion);
           }
        }
        
        // Spotify playlist controls
        if(ui_current_context().element.is($("#pg_sp1_playlist_overlay"))) {
            if (bid == C_buttonID.SQUARE) {
                spotify_shuffleOpenPlaylist();
            }
        }

        if (bid == C_buttonID.LT) {
            ui_navbar_slide(true);
        }
    }
}

function C_onButtonRelease(cid, bid) {
    if (!inputEnabled)
    {
        return;
    }
    
    if (bid == C_buttonID.LT) {
        ui_navbar_slide(false);
        if (navbar_current != current_page) {
            ui_loadpage();
        }
    }
}

function C_moveKeyboardCursor() {
    
    keyboard_pos_left_col = Math.floor(0.5+(C_axesPositions[keyboard_cid][0]+1)*2);
    keyboard_pos_left_row = Math.floor(0.5+(C_axesPositions[keyboard_cid][1]+1));
    
    keyboard_pos_right_col = Math.floor(5+0.5+(C_axesPositions[keyboard_cid][2]+1)*2);
    keyboard_pos_right_row = Math.floor(0.5+(C_axesPositions[keyboard_cid][3]+1));
    
    kb_select();
    
    $("#keyboard_selector_right").css({
        transform: "translateX(" + C_axesPositions[keyboard_cid][2]*50 + "%) translateY(" + C_axesPositions[keyboard_cid][3]*50 + "%)"
    });
    
    $("#keyboard_selector_left").css({
        transform: "translateX(" + C_axesPositions[keyboard_cid][0]*50 + "%) translateY(" + C_axesPositions[keyboard_cid][1]*50 + "%)"
    });   
}