var KEY = {
    URI: 0,
    LOCAL: 1,
    _PLAY: 2,
    SONG_FINISHED: 3,
    TIME: 4
};

//////////////////////////////////////////
// SPOTIFY
//////////////////////////////////////////

//var swh = require('node-spotify-webhelper').SpotifyWebHelper();
//
//oldURI = "";
//var nextForce = true;
//function pollSpotifyStatus()
//{   
//    swh.getStatus(function(err, res) {
//        data = res;
//        
//        // Uh-oh, data returned was not valid!
//        if (!data) return;
//        if (!data.track) return;
//        if (!data.track.artist_resource) return;
//        if (!data.track.track_resource) return;
//        
//        URI = data.track.track_resource.uri;
//        
//        // Track finished
//        if (data.playing_position > data.track.length - 1 || data.playing_position == 0) {
//            wss.broadcast(KEY.SONG_FINISHED);
//        }
//        
//        // Song has changed..
//        if (URI != oldURI || nextForce) {
//            
//            var obj = {
//                trackName: data.track.track_resource.name,
//                artistName: data.track.artist_resource.name
//            }
//            
//            if (URI.substr(8, 1) == "l") {
//                wss.broadcast(KEY.LOCAL, obj);
//            }
//            else {
//                obj.uri = URI;
//                wss.broadcast(KEY.URI, obj);
//            }
//            
//            var times = {
//                currentTime: data.playing_position,
//                duration: data.track.length
//            }
//            wss.broadcast(KEY.TIME, times);
//            
//            console.log(data.track.track_resource.name + " - " + data.track.artist_resource.name);
//            oldURI = URI;
//            nextForce = false;
//        }
//    });
//    
//    setTimeout(pollSpotifyStatus, 800);
//}
//pollSpotifyStatus();

var swh = require("@jonny/spotify-web-helper")();

swh.player.on("ready", function() {
    console.log("Connected to Spotify client.");
    
    swh.player.on("play", function() {
        console.log("RESUMED PLAYBACK");
    });
    
    // Pause, also finish song:
    swh.player.on("pause", function() {
        if(Math.abs(swh.status.track.length - swh.status.playing_position) < 4) {
            onSongFinish();
        }
    });
    
//    swh.player.on("end", function() {
//    });
    
    swh.player.on("track-change", function(data) {
        sendSongStatus(data);
        sendTime(true);
    });
    
    setInterval(function() {
        sendTime();
    }, 500);
});

function onSongFinish()
{
    wss.broadcast(KEY.SONG_FINISHED);
    console.log("TRACK FINISHED");
}

function sendSongStatus(data)
{
    var obj = {
        trackName: data.track_resource.name,
        artistName: data.artist_resource.name
    };

    if (data.track_type == "local") {
        wss.broadcast(KEY.LOCAL, obj);
    }
    else {
        obj.uri = data.track_resource.uri;
        wss.broadcast(KEY.URI, obj);
    }
}

function sendTime(zeroTime) {
    var times = {
        currentTime: zeroTime ? 0 : swh.status.playing_position,
        duration: swh.status.track.length
    }
    wss.broadcast(KEY.TIME, times);
}

///////////////////
// WEBSOCKET
//////////////////

var WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({
        port: 12965
    });

wss.on("connection", webSocketInit);

wss.broadcast = function (key, data) {
    for (var i in this.clients) {
        if (this.clients[i].readyState != this.clients[0].OPEN) {
            // DON'T SEND DATA TO CLIENTS NOT IN "OPEN" SOCKET STATE
        } else {
            this.clients[i].send(key, data);
        }
    }
};

function webSocketInit(ws) {
    console.log("Web client connected.");
    // use the key, data sending protocol.
    ws.sendDefault = ws.send.bind(ws);
    ws.send = function (key, data) {
        var object = {
            "key": key,
            "data": data
        };
        // remember: JSON.stringify breaks with circular structures, and spotify
        // node likes to return a bunch of those!! handle with care.
        ws.sendDefault(JSON.stringify(object));
    }
    
    // pass the message on to an external function
    // this is to avoid clustered brackets everywhere
    ws.on("message", function (messageObject) {
        receivedMessage(ws, JSON.parse(messageObject));
    });
    
    sendSongStatus(swh.status.track);
    sendTime();
}

function receivedMessage(ws, messageObject)
{
    var key = messageObject.key;
    var data = messageObject.data;
    
    switch (key) {
        case KEY._PLAY:
            swh.player.play(data);
            break;
        default: break;
    }
}